from process_data import x_train, x_validate , y_train, y_validate
from keras import Sequential
from keras.layers import Dense


# tao model gom 2 layer , layer 1 gom 16 neural , layer 2 gom 8 neural
model = Sequential()
model.add(Dense(16 ,input_dim=8 , activation='relu'))
model.add(Dense(12 ,activation='relu'))
model.add(Dense(8 ,activation='relu'))
model.add(Dense(1 , activation='sigmoid'))
model.summary()
# compile can luu y loss , optimizer , metrics là gì
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# train model epoch , batch_size là gì
model.fit(x_train , y_train , epochs=100 , batch_size= 2 , validation_data=(x_validate , y_validate))

# save model
model.save("myfirstModel.h5")


