
from keras.models import load_model
from process_data import x_test , y_test
import numpy

model_loaded = load_model('myfirstModel.h5')
loss , acc = model_loaded.evaluate(x_test , y_test)
print(f'(loss: {loss} , accuracy : {acc})')

#predict a specific data

x_new = x_test[1:10]
y_new = y_test[1:10]
# convert data before put in neural
x_new = numpy.expand_dims(x_new , axis=0)
y_predict = model_loaded.predict(x_new)

print(f'value predict: {y_predict}')
print(f'Actual value: {y_new}')
