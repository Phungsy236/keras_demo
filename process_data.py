from numpy import loadtxt
from sklearn.model_selection import train_test_split


dataset = loadtxt('data.csv', delimiter=',')

print(dataset.shape)
# chia data test and train , 1 row co 9 cot, 8 cot dau la x , cot cuoi la output
x = dataset[:, 0:8]
y = dataset[:, 8]

x_train_validate, x_test, y_train_validate, y_test = train_test_split(x, y, test_size=0.2)

x_train, x_validate , y_train, y_validate = train_test_split(x_train_validate, y_train_validate , test_size=0.2)
